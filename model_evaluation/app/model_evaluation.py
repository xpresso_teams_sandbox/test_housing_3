MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/test_housing_3/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    train_x_trans = pickle.load(open(f"{PICKLE_PATH}/train_x_trans.pkl", "rb"))
    labels_train = pickle.load(open(f"{PICKLE_PATH}/labels_train.pkl", "rb"))
    test_x_trans = pickle.load(open(f"{PICKLE_PATH}/test_x_trans.pkl", "rb"))
    labels_test = pickle.load(open(f"{PICKLE_PATH}/labels_test.pkl", "rb"))
    model = pickle.load(open(f"{PICKLE_PATH}/model.pkl", "rb"))
    model_scores = pickle.load(open(f"{PICKLE_PATH}/model_scores.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = model_evaluation
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH", "train_x_trans", "labels_train", "test_x_trans", "labels_test","model","model_scores"]




pred_train = model.predict(train_x_trans)

mse_train = mean_squared_error(labels_train, pred_train)

rmse_train = np.sqrt(mse_train)

mae_train = mean_absolute_error(labels_train, pred_train)


pred_test = model.predict(test_x_trans)

mse_test = mean_squared_error(labels_test, pred_test)

rmse_test = np.sqrt(mse_test)

mae_test = mean_absolute_error(labels_test, pred_test)

model_scores = pd.DataFrame({"metrics":["MSE","RMSE", "MAE" ],
    "train":[mse_train,rmse_train, mae_train ],
            "test":[mse_test,rmse_test, mae_test ]})

model_scores


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/test_housing_3/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(train_x_trans, open(f"{PICKLE_PATH}/train_x_trans.pkl", "wb"))
    pickle.dump(labels_train, open(f"{PICKLE_PATH}/labels_train.pkl", "wb"))
    pickle.dump(test_x_trans, open(f"{PICKLE_PATH}/test_x_trans.pkl", "wb"))
    pickle.dump(labels_test, open(f"{PICKLE_PATH}/labels_test.pkl", "wb"))
    pickle.dump(model, open(f"{PICKLE_PATH}/model.pkl", "wb"))
    pickle.dump(model_scores, open(f"{PICKLE_PATH}/model_scores.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
